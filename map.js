var map = L.map('map', {
		minZoom: 7,
		maxZoom: 13});

function niceDate(value) {
	var date = value.split('.')
	var day = parseInt(date[0])
	var mnt = parseInt(date[1])
	var yr = parseInt(date[2])

	return day + '. ' + mnt + '. ' + yr
};

map.scrollWheelZoom.disable();

var background = L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
	opacity: 1,
    attribution: 'mapová data © přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>,' 
    + ' zdroj dat <a target="_blank" href="http://www.birds.cz/avif/">Faunistická databáze ČSO</a>'
});

var labels = L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
	opacity: 1
});

var datel = L.markerClusterGroup();

$.getJSON( "./datel.json", function(data) {
  for (feature in data['features']) {
  	var ftr = data['features'][feature]
  	var marker = L.marker(new L.LatLng(ftr.geometry.coordinates[1], ftr.geometry.coordinates[0]), {ftr});
  	marker.bindPopup('Datel černý byl poblíž obce <b>' 
  		+ ftr.properties.NearestMun + '</b> pozorován ' + niceDate(ftr.properties.datum) + '<br>'
  		+ 'Detaily <a target="_blank" href="' + ftr.properties.Link + '">v databázi AVIF</a>.', {
  			maxWidth : 560
  		});
  	datel.addLayer(marker)
  }
});

var form = document.getElementById("frm-geocode");
var geocoder = null;
var geocodeMarker = null;
form.onsubmit = function(event) {
	if (!geocoder) {
		geocoder = new google.maps.Geocoder();
	}
	event.preventDefault();
	var text = document.getElementById("inp-geocode").value;

	geocoder.geocode({'address':text}, function(results, status) {
		if(status !== google.maps.GeocoderStatus.OK) {
			alert("Bohužel, danou adresu nebylo možné najít");
			return;
		}
		var result = results[0];
		var latlng = new L.LatLng(result.geometry.location.lat(), result.geometry.location.lng());
		map.setView(latlng, 15);
	});
};

map.setView([49.7417517, 15.3350758], 8)
			.addLayer(background)
			.addLayer(datel)
			.addLayer(labels)			